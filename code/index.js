
function Calculator(){	
	this.read = function(){
		this.x = + prompt("Enter the first value:");
		this.y = + prompt("Enter the second value:");				
	}
	this.say = function(){
		return document.write("Entered values: " + this.x + " and " + this.y + '<br>');
	}	
	this.sum = function(){
		return document.write("Sum of values: " + (this.x + this.y) + '<br>');
	}
	this.mul = function(){
		return document.write("Multiplication of values: " + this.x * this.y);
	}
}
let a = new Calculator();

a.read();
a.say();
a.sum();
a.mul();